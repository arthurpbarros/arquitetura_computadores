main:	addi $2,$0,5
	syscall
	add $t9,$0,$2
	
	addi $t8,$0,0 #contador_de_linhas
	addi $t6,$0,0 #contador_de_colunas
	addi $t7,$0,1  #numero
	
	
loop_coluna:	beq $t8,$t9, fim
			add $t3,$t8,1
loop_linha:		beq $t6,$t3,fim_linha
				j escrever
			
fim_linha:	addi $t8,$t8,1
		j escrever_linha
		
	
escrever:	add $4,$0,$t7
		add $2,$0,1
		syscall
		add $4,$0,' '
		add $2,$0,11
		syscall
		addi $t7,$t7,1 #incrementa $t7
		addi $t6,$t6,1 #incrementa $t6
		j loop_linha
		
escrever_linha:		add $4,$0,'\n'
			add $2,$0,11
			syscall
			addi $t6,$0,0 #zera_de_colunas
			j loop_coluna
		
fim:	addi $2,$0,10
	syscall
