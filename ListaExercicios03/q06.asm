#Fa�a um programa que leia n�meros inteiros e calcule a soma. O la�o do programa
#termina quando o usu�rio digita um valor negativo. Em seguida o programa imprime a
#soma dos valores digitados.
main: 		addi $t3,$0,0 #soma dos valores
	
ler_num:	add $2,$0,5
		syscall
		addi $t0,$2,0 #numero lido
		
		slt $t1,$t0,$0
		bne $t1,$0,imp_soma
		add $t3,$t3,$t0 #soma = soma + numero lido
		j ler_num
imp_soma:	add $4,$t3,0
		addi $2,$0,1
		syscall
fim:		add $2,$0,10
		syscall
	
