main: 
	add $2,$0,5
	syscall
	addi $s0,$2,0

	add $2,$0,5
	syscall
	addi $s6,$2,0 #X
	
	add $2,$0,5
	syscall
	addi $s7,$2,0#Y
	
loop:   beq $s0,$0, fim
	addi $s1,$s0,0 #contador
loop_leituras:	beq $s0,$0, fimloop

		add $2,$0,5
		syscall
		addi $t0,$2,0   #->X
		
		add $2,$0,5
		syscall
		addi $t1,$2,0   #->Y
		
		j calcula

voltar:		addi $s1,$s1,-1
		j loop_leituras
	
		
fimloop:      	addi $s0,$s0,-1
		j loop

fim:	addi $2,$0,10
	syscall
	
calcula:	beq $t0,$s6,imprimeDV
		beq $t1,$s7,imprimeDV
		
		slt $t2,$s6,$t0  #1 se s6 < t0
		slt $t3,$s7,$t1	#1 se s7 < t1
		
		and $t4,$t2,$t3	
		bne $t4,$0,imprimeNE
		or $t4,$t2,$t3
		beq $t2,$0,imprimeSO
		
		bne $t2,$0,imprimeSE
		j imprimeNO



imprimeSE:	add $2,$0,11
		add $4,$0,'S'
		syscall
		add $4,$0,'E'
		syscall
		add $4,$0,'\n'
		syscall
		j voltar


imprimeSO:	add $2,$0,11
		add $4,$0,'S'
		syscall
		add $4,$0,'O'
		syscall
		add $4,$0,'\n'
		syscall
		j voltar



imprimeNO:	add $2,$0,11
		add $4,$0,'N'
		syscall
		add $4,$0,'O'
		syscall
		add $4,$0,'\n'
		syscall
		j voltar


imprimeNE:	add $2,$0,11
		add $4,$0,'N'
		syscall
		add $4,$0,'E'
		syscall
		add $4,$0,'\n'
		syscall
		j voltar

imprimeDV:	add $2,$0,11
		add $4,$0,'D'
		syscall
		add $4,$0,'V'
		syscall
		add $4,$0,'\n'
		syscall
		j voltar
	
