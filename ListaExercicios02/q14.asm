.text
main:	addi $2,$0,5
	syscall
	addi $t0,$2,0 #numero1
	
	addi $2,$0,5
	syscall
	addi $t5,$2,0 #numero2
	addi $t8,$0,1 #serve pra imprimir v�rgula
	
	addi $t9,$0,10
	div $t0,$t9
	mfhi $t1
	mflo $t0
	
	div $t0,$t9
	mfhi $t2
	mflo $t0
	
	div $t0,$t9
	mfhi $t3
	mflo $t4
	
inicio:	addi $t9,$0,0
	beq $t4,$0, imprime_u_m
	addi $t9,$0,1 #1. indica que 4_algarismo � diferente de 0
	add $4,$0,$t4
	add $4,$4,16
	
imprime_u_m: 	addi $4,$4,32
		addi $2,$0,11
		syscall
		bne $t9,$0, imprime_c #se 4 algarismo for diferente de 0, imprima o 3 algarismo, sen�o se algarismo for 0 imprima 1
		addi $t9,$0,0
		beq $t3,$0, imprime_0_car
		addi $t9,$0,1 #1. Indica que o 3_algarismo � diferente de 0
		j imprime_c
		
imprime_0_car:  sub $t3,$t3,16		
imprime_c:	addi $4,$t3,48
		syscall
		bne $t9,$0, imprime_d #se algarismo 4 ou 3 for diferente de 0, imprima 2 algarismo
		addi $t9,$0,0
		beq $t2,$0, imprime_0_car2
		addi $t9,$0,1
		j imprime_d
imprime_0_car2:	sub $t2,$t2,16
imprime_d:	addi $4,$t2,48
		syscall
imprime_u:	add $4,$0,$t1
		add $4,$4,48
		syscall
		beq $t8,$0, fim
imprime_virgula:	add $4,$0,','
			syscall
			j num2
fim:	addi $2,$0,10
	syscall
	
num2:	addi $t9,$0,10 #Constante 10
	div $t5,$t9
	mfhi $t1
	mflo $t5
	
	div $t5,$t9
	mfhi $t2
	mflo $t5
	
	div $t5,$t9
	mfhi $t3
	mflo $t4
	
	addi $t8,$0,0 #para nao imprimir a virgula
	addi $4,$0,0 #para nao interferir no c�lculo
	j inicio
	
	
		
	