#Fa�a um programa para calcular o MDC de dois n�meros fornecidos pelo usu�rio,
#usando o algoritmo de Euclides (busque na Internet o funcionamento desse
#algoritmo).

main:	addi $2,$0,5
	syscall
	addi $t0,$2,0 #a
	
	addi $2,$0,5
	syscall
	addi $t1,$2,0 #b
loop:	beq $t1,$0,imprimeMDC	
	add $t2,$0,$t0 #aux = a
	addi $t0,$t1,0    #a = b
	div $t2,$t1 #copia de a/b
	mfhi $t1
	j loop

imprimeMDC: 	
		beq $t0,$t1,imprimeMDC1	#Se os dois n�meros forem 0, o MDC deles � 1
		addi $2,$0,1
		add $4,$0,$t0
		syscall
		j fim
imprimeMDC1:	addi $t0,$0,1
		j imprimeMDC
fim:		addi $2,$0,10
		syscall
		
