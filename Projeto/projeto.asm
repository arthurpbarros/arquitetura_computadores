.data
.text
#posso usar t5,t6,t8
main:
	#lui $2,0x1001 #primeiro endere�o
	addi $t0,$0,1024 #largura (sempre m�ltiplo de 4)
	addi $t1,$0,512 #altura  (sempre m�ltiplo de 4)
	addi $t3,$0,4   #tamanho da unidadegr�fica
	
	addi $s2,$0,-4 #1acordaPosicionada a direita
	addi $s4,$0,-1
	addi $s5,$0,4
	
	jal PixelsAPintar
	lui $4, 0x1001  #Endere�o do primeiro pixel
	addi $t2,$0,0
	ori $t2, 0x000f7061 #Cor Verde Escuro
	jal fundo 	#pintar fundo
	
	addi $t1,$0,48
	jal PixelsAPintar
	lui $4, 0x1001  #Endere�o do primeiro pixel
	addi $t2,$0,0
	ori $t2, 0x00bf01 #Cor Verde 
	jal fundo	#pintar parte de cima da floresta (�rvores)
	
	addi $t1,$0,32
	jal PixelsAPintar
	#$4 j� est� pronto, pois � o �ltimo da fun��o anterior, a cor tamb�m
	jal detalhesVeg
	addi $s7,$4,0
	addi $t1,$0,0 #boneco na corda 2
loop:	
	
	addi $s6,$s7,0
	
	mul $s3,$s2,$s4 #Cordas invertidas s3 refere a 2corda, s2 a primeira corda
	
	add $t5,$s6,0 #1 pixel a ser apagado
	
	add $t9,$0,$t1
	addi $6,$0,208 #primeira coluna da corda 1
	addi $s1,$s2,0
	jal cordas

	bne $t1,$0,ver0P #se t1 = 1, desenha o boneco
	j cor2
	
ver0P:	beq $s3,$0,chamaBMP #s2 == 0
	j ver1P
chamaBMP:	jal bonecoMeio
ver1P:   beq $s3,$s5,chamaBDP #s2 == 4
	j ver2P	
chamaBDP:	jal bonecoDir
ver2P:	beq $s3,$0,cor2
	bne $s3,$s5,chamaBEP #s2 == -4
	j cor2
chamaBEP: 	jal bonecoEsq

#negar t1
cor2:	not $t1,$t1
	andi $t1,$t1,1
	
	
	
	add $t9,$0,$t1
	addi $6,$0,808 #primeira coluna da corda 2
	addi $s1,$s3,0
	jal cordas

	bne $t1,$0,ver0 #se t1 = 1, desenha o boneco
	j cursoNormal
	
ver0:	beq $s3,$0,chamaBM #s2 == 0
	j ver1
chamaBM:	jal bonecoMeio
ver1:   beq $s3,$s5,chamaBD #s2 == 4
	j ver2	
chamaBD:	jal bonecoDir
ver2:	beq $s3,$0,cursoNormal
	bne $s3,$s5,chamaBE #s2 == -4
	j cursoNormal
chamaBE: 	jal bonecoEsq
			
cursoNormal:						
	addi $t4,$0,70
	mul $t4,$t4,$t0
	add $s6,$s6,$t4
	addi $t6,$s6,0 #�ltimo pixel a ser apagado -4
	addi $t2,$0,0
	ori $t2,0x00728C21
	jal Campo
	
#Ler tecla do usu�rio	posso usar t5 t3
lerTecl: lui $7, 0xffff
	lw $5, 0($7)
	bne $5, $0, verCaracter
verCaracter: lw $3, 4($7)
	addi $4,$0,' '
	bne $3,$4,bonecoPula
	#j bonecoPula
	j delay
bonecoPula:
	not $t1,$t1 #0 -> 11111111 & 00000001 -> 00000001 -> 1 / 1-> 11111110 & 00000001 -> 00000000 -> 0 
	andi $t1,$t1,1
	
	
	
delay:	
	sw $0,4($7)
#Delay
	addi $2,$0,32
	addi $4,$0,300
	syscall

	
	jal apagarCentro
	
	
	beq $s2,$s5,atualizaCorda1
	addi $s2,$s2,4
	j loop
atualizaCorda1:		
	addi $s2,$0,-4
	j loop
	
	
	j fim	
	
apagarCentro:	
			addi $t2,$0,0
			ori $t2, 0x000f7061
lac:			sw $t2,0($t5)
			beq $t5,$t6,fimapagarCentro
			addi $t5,$t5,4
			j lac
fimapagarCentro:	jr $31

# Fun��o Pixeis a pintar: 
# 	Retorna a quantidade de pixels que se deseja pintar
# Entrada: 
#		$t0: A largura em pixels da �rea  
# 		$t1: A altura em pixels da �rea 
# Sa�da:
#		$5 
#Preserva: $t0,$t1
#N�o Preserva: $5,$t4
PixelsAPintar:
		mul $t4,$t0,$t1
		srl $t4,$t4,4
		add $5,$0,$t4
fimPixelsAPintar: jr $31
# Fun��o Fundo: Pinta o fundo em alguma cor especifica
# Entrada: 
#		$4: Endere�o do primeiro pixel  
# 		$t2: A cor do pixel 
# Sa�da:
#		
#Preserva: $t2
#N�o Preserva: $4,$5
fundo:		
		sw $t2,0($4) #primeiro endere�o recebe a cor verde
		addi $5,$5,-1 #decrementa quantidade
		addi $4,$4,4 #aumenta em 4 bits
		bne $5,$0, fundo
fimFundo:	jr $31

# Fun��o Fundo: Pinta o fundo em alguma cor especifica
# Entrada: 
#		$6: Endere�o do primeiro pixel  
# 		$t2: A cor do pixel 
# Sa�da:
#		
#Preserva: $t2
#N�o Preserva: $4,$5
detalhesVeg:	
		addi $t3,$4,0
		addi $t5,$0,10
		
		div $5,$t5
		mfhi $t6
		addi $t7,$0,5
		slt $t8,$t6,$t7
		bne $t8,$0,pinta
		j naoPinta
pinta:		
		sw $t2,0($4) #primeiro endere�o recebe a cor verde
naoPinta:
		addi $5,$5,-1 #decrementa quantidade
		addi $4,$4,4 #aumenta em 4 bits
		bne $5,$0, detalhesVeg
fimdetalhesVeg: jr $31

#Fun��o Cordas: Desenha as cordas na imagem a partir de uma coluna com tamanho de 40 linhas
#	Recebe:	$4 -> Primeiro elemento da primeira linha onde se ir� desenhar a corda
# 		$6 -> Coluna em que se desenhar� os pixeis da corda
#		$5 -> Quantidade de linhas da corda, comprimento
#		$t2 -> Cor da corda
#	Preserva: $4
#	N�o preserva: $6,$5



#Corda no centro
cordas:		
		addi $t3,$0,5
		addi $t2,$0,0	   
		ori $t2,0x002C1C0F #cor marrom
		add $6,$6,$s6 #primeiro endere�o onde se inicia a corda
		beq $s1,$0,aumentacorda
		j cordainclinada
aumentacorda:	addi $5,$0,40
		j cordaloop

cordainclinada:	addi $5,$0,35 #linhas da corda, comprimento
cordaloop:	
		#if pixel = vermelho, nao pintar
		addi $t8,$0,0
		ori $t8,0x00EEBB98
		
		
		#lw $t2,
		#bne $t2,$t8,pintaCorda
		#j naopintaCorda
		sw $t2,0($6)
		addi $5,$5,-1
		beq $5,$t3,chamadaboneco
		j retornoDesenhoBoneco
chamadaboneco:	bne $t9,$0, guardaEndBoneco
retornoDesenhoBoneco: add $6,$6,$t0 #soma a quantidade de pixels de largura para se chegar na pr�xima coluna
		      add $6,$6,$s1 #s1 � o deslocamento para direita 4, esquerda -4 ou nenhum 0
		bne $5,$0, cordaloop
fimcordas:	jr $31

#Corda inclinada para direita


guardaEndBoneco:	
		addi $s0,$6,0 #ENDERE�O BASE DO BONECO
		addi $t9,$0,0
		j retornoDesenhoBoneco
#Fun��o Boneco : Desenha o boneco a partir de um uma coluna


bonecoDir:		
		addi $t4,$0,5
		mul $t4,$t4,$t0
		
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelha -> chap�u	
		sub $t7,$s0,$t4
		sw $t2,20($t7)
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00484D50 #cor cinza -> olho
		sw $t2,24($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> cabe�a
		#sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,20($t7)
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		
		
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> bra�o	
		add $t7,$t7,$t0
		sw $t2,0($t7)
		sw $t2,4($t7)
		addi $t2,$0,0
		#FF8027	   
		ori $t2,0x00dd0000 #cor vermelho -> roupa
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> abdomen
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelho -> short
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> perna
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		#sw $t2,28($t7)
		sw $t2,40($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		#sw $t2,28($t7)
		sw $t2,36($t7)
		sw $t2,40($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		sw $t2,36($t7)
		sw $t2,40($t7)
		add $t7,$t7,$t0
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		sw $t2,40($t7)
		
		addi $t2,$0,0	   
		ori $t2,0x002C1C0F #cor marrom

fimbonecoDir:		jr $31

bonecoMeio:		
		addi $t4,$0,5
		mul $t4,$t4,$t0
		
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelha -> chap�u	
		sub $t7,$s0,$t4
		sw $t2,20($t7)
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00484D50 #cor cinza -> olho
		sw $t2,24($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> cabe�a
		#sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,20($t7)
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,24($t7)		
		sw $t2,28($t7)
		sw $t2,32($t7)
		
		
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> bra�o	
		add $t7,$t7,$t0
		sw $t2,0($t7)
		sw $t2,4($t7)
		addi $t2,$0,0
		#FF8027	   
		ori $t2,0x00dd0000 #cor vermelho -> roupa
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> abdomen
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelho -> short
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		sw $t2,32($t7)
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> perna
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		addi $t2,$0,0	   
		ori $t2,0x00D1A486 #cor de pele escura -> perna
		sw $t2,28($t7)
		#sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> perna
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		addi $t2,$0,0	   
		ori $t2,0x00D1A486 #cor de pele escura -> perna
		sw $t2,28($t7)
		#sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> perna
		add $t7,$t7,$t0
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		addi $t2,$0,0	   
		ori $t2,0x00D1A486 #cor de pele escura -> perna
		sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> perna
		add $t7,$t7,$t0
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		#addi $t2,$0,0	   
		#ori $t2,0x00D1A486 #cor de pele escura -> perna	
		#sw $t2,28($t7)
		addi $t2,$0,0	   
		ori $t2,0x002C1C0F #cor marrom

fimbonecoMeio:		jr $31
bonecoEsq:
		addi $t4,$0,5
		mul $t4,$t4,$t0
		
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelha -> chap�u	
		sub $t7,$s0,$t4
		sw $t2,12($t7)
		sw $t2,16($t7)		
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)		
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00484D50 #cor cinza -> olho
		sw $t2,16($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> cabe�a
		#sw $t2,24($t7)		
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		sw $t2,-8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)		
		sw $t2,24($t7)
		add $t7,$t7,$t0
		sw $t2,-4($t7)
		addi $t2,$0,0
		ori $t2,0x00EEBB98
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> bra�o	
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelho -> roupa
		sw $t2,0($t7)
		sw $t2,4($t7)
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		sw $t2,4($t7)
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> abdomen
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelho -> short
		add $t7,$t7,$t0
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		add $t7,$t7,$t0
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> perna
		sw $t2,-4($t7)
		addi $t2,$0,0	   
		ori $t2,0x00dd0000 #cor vermelho -> short
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		sw $t2,24($t7)
		addi $t2,$0,0	   
		ori $t2,0x00EEBB98 #cor de pele -> perna
		add $t7,$t7,$t0
		sw $t2,-4($t7)
		sw $t2,0($t7)
		sw $t2,4($t7)
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		add $t7,$t7,$t0
		sw $t2,-4($t7)
		sw $t2,0($t7)
		sw $t2,4($t7)
		sw $t2,8($t7)
		sw $t2,12($t7)
		sw $t2,16($t7)
		sw $t2,20($t7)
		
		
		
		addi $t2,$0,0	   
		ori $t2,0x002C1C0F #cor marrom

fimbonecoEsq:		jr $31

Campo:	
	addi $t8,$0,268632064
	sw $t2,0($s6)	#sw $t2,0($4)
	addi $s6,$s6,4	#addi $4,$4,4
	beq $s6,$t8,fimCampo #beq $4,$t8,fimCampo
	j Campo
fimCampo: jr $31


fim:	addi $2,$0,10
	syscall
