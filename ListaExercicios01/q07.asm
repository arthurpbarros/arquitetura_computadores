.text
main:	add $2,$0,5
	syscall
	addi $t9,$0,10		#Constante 10
	add $t0,$0,$2		#Número lido
	div $t0,$t9		#Divisão por 10
unid:	mfhi $t3		#Guarda algarismo das unidades (resto)
	mflo $t0
	div $t0,$t9
dez:	mfhi $t4		#Guarda algarismo das dezenas (resto)
	mflo $t0
	div $t0,$t9
cen:	mfhi $t5		#Guarda algarismo das centenas (resto)
	mflo $t0
	div $t0,$t9
mil:	mfhi $t6
	
imprime:	add $4,$0,$t3
		addi $2,$0,1
		syscall
		
		addi $4,$0,'\n'
		addi $2,$0,11
		syscall
		
		add $4,$0,$t4
		addi $2,$0,1
		syscall
		
		addi $4,$0,'\n'
		addi $2,$0,11
		syscall
		
		add $4,$0,$t5
		addi $2,$0,1
		syscall
		
		addi $4,$0,'\n'
		addi $2,$0,11
		syscall
		
		add $4,$0,$t6
		addi $2,$0,1
		syscall
		
		addi $2,$0,10
		syscall
		

