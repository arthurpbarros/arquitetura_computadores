.text
main:	addi $2,$0,5
	syscall
	addi $t0,$2,0 #numero
	
	addi $t7,$0,1 #acumulador 
	addi $t8,$0,2 #divisor
	addi $t9,$0,0 #valor_binario
	addi $t6,$0,10 #constante 10
	
dividir: 	slt $t5,$t0,$t8 #1, se numero < 2
		bne $t5,$0, fim
		div $t0,$t8
		mflo $t0
		mfhi $t1 #resto
		mul $t1,$t1,$t7
		add $t9,$t9,$t1
		mul $t7,$t7,$t6
		j dividir

fim:	mul $t2,$t0,$t7
	add $4,$t9,$t2
	addi $2,$0,1
	syscall
	addi $2,$0,10
	syscall
	
		
