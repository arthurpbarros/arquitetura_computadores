main:	addi $2,$0,5
	syscall
	addi $t9,$2,0 #recebe n do usu�rio

descarte:
	#Descarta n�meros �mpares 
	#Soma = 2a.(a+b) -> 2 (a.(a+b) -> Sempre � um n�mero par
	addi $t3,$0,2
	div $t9,$t3
	mfhi $t3
	bne $t3,$0, naoExiste	
	
	#Descarta n�meros menores que 12 (a menor soma de um terno)
	addi $t3,$0,12
	slt $t6,$t9,$t3
	bne $t6,$0, naoExiste
	
	
	
	addi $t0,$0,2 #m
	addi $t1,$0,1 #n

loop:	

	mul $t6,$t0,$t0
	mul $t7,$t1,$t1	
	sub $t6,$t6,$t7 #a
	
	mul $t7,$t0,$t1
	sll $t7,$t7,1 #b
	
	mul $t3,$t0,$t0
	mul $t8,$t1,$t1
	add $t8,$t3,$t8 #c
	
	add $t4,$0,$t6 
	add $t4,$t4,$t7
	add $t4,$t4,$t8 #$t4 = a+b+c
	
	beq $t4,$t9, imprimeTerno #numero = a+b+c
	addi $t2,$t0,-1
	beq $t1,$t2, proximo #n == m-1
	#addi $t1,$t1,1 #aumenta n em 1 3,1 / 3,2 -> 4,1 / 4,2 / 4,3
		#se necess�rio pula n
	addi $t1,$t1,1
	j loop
	
proximo:	
		
		sll $t2,$t9,1
		slt $t5,$t2,$t4 #1 se n�mero*2 < soma
		bne $t5,$0,naoExiste		
		
		addi $t0,$t0,1
		addi $t1,$0,1
		j loop

imprimeTerno:	add $2,$0,11
		addi $4,$0,'A'
		syscall
		addi $4,$0,'='
		syscall
		add $2,$0,1
		add $4,$0,$t6
		syscall
		add $2,$0,11
		addi $4,$0,','
		syscall
		addi $4,$0,'B'
		syscall
		addi $4,$0,'='
		syscall
		add $2,$0,1
		add $4,$0,$t7
		syscall
		add $2,$0,11
		addi $4,$0,','
		syscall	
		addi $4,$0,'C'
		syscall
		addi $4,$0,'='
		syscall
		add $2,$0,1
		add $4,$0,$t8
		syscall
		j fim


naoExiste:	addi $2,$0,11
		add $4,$0,'N'
		syscall
		add $4,$0,'�'
		syscall
		add $4,$0,'O'
		syscall
		add $4,$0,' '
		syscall
		add $4,$0,'E'
		syscall
		add $4,$0,'X'
		syscall
		add $4,$0,'I'
		syscall
		add $4,$0,'S'
		syscall
		add $4,$0,'T'
		syscall
		add $4,$0,'E'
		syscall
		add $4,$0,' '
		syscall
		add $4,$0,'T'
		syscall
		add $4,$0,'E'
		syscall
		add $4,$0,'R'
		syscall
		add $4,$0,'N'
		syscall
		add $4,$0,'O'
		syscall
		
fim:		addi $2,$0,10
		syscall
