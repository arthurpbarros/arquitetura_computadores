.text
main: 	addi $2,$0,5
	syscall
	
	add $t1,$0,$2 #N�mero lido
	
	sub $t0,$t1,$0
	srl $t0,$t0,31 #Verifica��o do sinal do n�mero
	
	beq $t1,$0, fim #N�mero = 0, nem positivo nem negativo
	beq $t0,$0, positivo
	j negativo
	
positivo: 	sll $4,$t1,1
		j imprime
negativo:	mul $4,$t1,$t1
		j imprime
imprime: 	addi $2,$0,1
		syscall
		
fim:		addi $2,$0,10
		syscall