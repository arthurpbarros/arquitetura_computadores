#Fa�a um programa leia certa quantidade de n�meros e imprima o maior deles e
#quantas vezes o maior n�mero foi lido. A quantidade de n�meros a serem lidos deve
#ser fornecido pelo usu�rio.
main:	
	addi $2,$0,5
	syscall
	addi $t9,$2,0 #quantidade de numeros a serem lidos
	addi $t8,$0,0 #contador de numeros lidos
	addi $t6,$0,0 #maior numero lido
	addi $t7,$0,0 #quantidade de vezes do maior n�mero

ler_n:	beq $t8,$t9, imprimeMaior
	addi $2,$0,5
	syscall
	addi $t0,$2,0 #numero lido
	beq $t8,$0,  maior_recebe_n #executado apenas na 1 vez
	addi $t8,$t8,1 #contador += 1
verifica_maior:	
	slt $t2,$t6,$t0 #1, se $t6<$t0
	bne $t2,$0,atualiza_maior_1vez
	beq $t0,$t6,aumenta_vezes
	j ler_n

atualiza_maior_1vez:	addi $t6,$t0,0 #maior = n�mero
			addi $t7,$0,1
			j ler_n
aumenta_vezes:		addi $t7,$t7,1 #incrementa o n�mero de vezes
			j ler_n
	
maior_recebe_n:	add $t6,$0,$t0
		addi $t7,$0,1 #numero de vezes lido = 1
		addi $t8,$t8,1
		j ler_n

semNumeros:	
		addi $4,$0,'N'
		syscall
		addi $4,$0,'Ã'
		syscall
		addi $4,$0,'0'
		syscall
		addi $4,$0,' '
		syscall
		addi $4,$0,'H'
		syscall
		addi $4,$0,'Á'
		syscall
		addi $4,$0,' '
		syscall
		addi $4,$0,'M'
		syscall
		addi $4,$0,'A'
		syscall
		addi $4,$0,'I'
		syscall
		addi $4,$0,'O'
		syscall
		addi $4,$0,'R'
		syscall 
		j fim
imprimeMaior:	
		addi $2,$0,11 #seta escrita de caracteres para ambos os metodos de escrita
		beq $t9,$0,semNumeros
		addi $4,$0,'M'
		syscall
		addi $4,$0,'A'
		syscall
		addi $4,$0,'I'
		syscall
		addi $4,$0,'O'
		syscall
		addi $4,$0,'R'
		syscall
		addi $4,$0,' '
		syscall
		addi $4,$0,'='
		syscall
		addi $4,$0,' '
		syscall
		addi $2,$0,1
		add $4,$0,$t6
		syscall
		addi $2,$0,11
		addi $4,$0,'\n'
		syscall
		addi $4,$0,'V'
		syscall
		addi $4,$0,'E'
		syscall
		addi $4,$0,'Z'
		syscall
		addi $4,$0,'E'
		syscall
		addi $4,$0,'S'
		syscall
		addi $4,$0,' '
		syscall
		addi $4,$0,'='
		syscall
		addi $4,$0,' '
		syscall
		addi $2,$0,1
		add $4,$0,$t7
		syscall
		
fim:		addi $2,$0,10
		syscall
		