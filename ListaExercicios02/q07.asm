.text
main: 	addi $2,$0,5
	syscall
	
	addi $t9,$0,400
	div $2,$t9
	mfhi $t8
	
	beq $t8,$0, bis
ver_4:	addi $t9,$0,4
	div $2,$t9
	mfhi $t8
	
	beq $t8,$0, ver_100
	j nbis
ver_100: 	addi $t9,$0,100
		div $2,$t9
		mfhi $t8
		bne $t8,$t0, bis
		j nbis

bis:	addi $4,$0,'S'
	addi $2,$0,11
	syscall
	j fim

nbis: 	addi $4,$0,'N'
	addi $2,$0,11
	syscall
	
fim: 	addi $2,$0,10
	syscall
