main: 	#addi $2,$0,5
	#syscall
	addi $t0,$2,3 #numero de digitos de um n�mero
	
addi	$t1,$0,9
addi 	$t2,$0,0 #soma = numero mais alto com n algarismos
addi 	$t3,$0,1 #multiplicador
addi 	$t4,$0,0 #contador
addi 	$s0,$0,10 #constante 10
maiorNumero:	beq $t4,$t0, buscarPal
		mul $t9,$t1,$t3
		add $t2,$t2,$t9
		
		mul $t3,$t3,$s0
		addi $t4,$t4,1 #incremento
		j maiorNumero
		
buscarPal:
div	$t3,$s0
mflo	$t1 #numero mais baixo com n algarismos
addi 	$t1,$t1,-1 #

add $t8,$0,$t3 #divisor do palindromo
add $t3,$0,$t2 #i do N1
lacoN1:	beq $t3,$t1, fim
	add $t4,$0,$t2 #i do N2	
	lacoN2:	beq $t4,$t1, incLacoN1
		mul $t5,$t3,$t4 #produto
		add $s7,$0,$t5
		j palindromo
cont:		addi $t4,$t4,-1
		j lacoN2

incLacoN1:
	addi $t3,$t3,-1
	j lacoN1
	
palindromo: 
	addi $s0,$0,100000
	div $t5,$s0
	mflo $s0
	
	slt $s1,$0,$s0
	beq $s1,$0, cont
	
	addi $s0,$0,10
	div $t5,$s0
	mfhi $s1
	mflo $t5
	
	div $t5,$s0
	mfhi $s2
	mflo $t5
	
	div $t5,$s0
	mfhi $s3
	mflo $t5
	
	div $t5,$s0
	mfhi $s4
	mflo $t5
	
	div $t5,$s0
	mfhi $s5
	mflo $s6
	
	beq $s1,$s6, c25
	j cont
c25:	beq $s2,$s5, c34
	j cont
c34:	beq $s3,$s4, imprime
	j cont

imprime:  	addi $2,$0,1
		add $4,$0,$s7
		syscall
		
		#Resposta 580085 = 995 * 583
fim:		addi $2,$0,1
		syscall