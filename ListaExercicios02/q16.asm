.text
main:	addi $2,$0,5
	syscall
	addi $t0,$0,10
	add $s0,$0,$2
	div $2,$t0 #ultimo digito
	mfhi $t1
	mflo $2
	
	div $2,$t0
	mfhi $t2
	mflo $2
	
	div $2,$t0
	mfhi $t3
	mflo $2
	
	div $2,$t0
	mfhi $t4
	mflo $2
	
	div $2,$t0
	mfhi $t5
	mflo $2
	
	div $2,$t0
	mfhi $t6
	mflo $2
	
	div $2,$t0
	mfhi $t7
	mflo $2
	
	div $2,$t0
	mfhi $t8
	mflo $t9 #primeiro digito
	
cal_d10:  	sll $s1,$t1,1
		mul $s2,$t2,3
		add $s1,$s1,$s2
		sll $s2,$t3,2
		add $s1,$s1,$s2
		mul $s2,$t4,5
		add $s1,$s1,$s2
		mul $s2,$t5,6
		add $s1,$s1,$s2
		mul $s2,$t6,7
		add $s1,$s1,$s2
		sll $s2,$t7,3
		add $s1,$s1,$s2
		mul $s2,$t8,9
		add $s1,$s1,$s2
		mul $s2,$t9,10
		add $s1,$s1,$s2
		
		addi $t0,$0,11
		div $s1,$t0
		mfhi $s1
		
		addi $s4,$0,2
		slt $t0,$s1,$s4
		beq $t0,$0, imprime_10_d
imprime_0:   	addi $4,$0,0
		addi $s1,$0,0 #10 digito salvo
		addi $2,$0,1
		syscall
		j cal_d11
imprime_10_d:	addi $t0,$0,11
		sub $s1,$t0,$s1 #10 digito salvo
		addi $4,$s1,0
		addi $2,$0,1
		syscall
cal_d11:	
		sll $s2,$s1,1
		mul $s3,$t1,3
		add $s3,$s3,$s2
		sll $s2,$t2,2
		add $s3,$s3,$s2
		mul $s2,$t3,5
		add $s3,$s3,$s2
		mul $s2,$t4,6
		add $s3,$s3,$s2
		mul $s2,$t5,7
		add $s3,$s3,$s2
		sll $s2,$t6,3
		add $s3,$s3,$s2
		mul $s2,$t7,9
		add $s3,$s3,$s2
		mul $s2,$t8,10
		add $s3,$s3,$s2
		mul $s2,$t9,11
		add $s3,$s3,$s2
		
		add $t0,$0,11
		div $s3,$t0
		mfhi $s3 #11 digito
		
		slt $t0,$s3,$s4
		beq $t0,$0, imprime_11_d
imprime_0_11d:	addi $4,$0,0
		addi $2,$0,1
		syscall
		j fim
imprime_11_d:	addi $t0,$0,11
		sub $4,$t0,$s3 
		addi $2,$0,1
		syscall
fim: 		addi $2,$0,10
		syscall
		
	
	
	
	