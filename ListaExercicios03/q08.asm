#Fa�a um programa que leia n�meros inteiros diferentes de zero e encontre o menor
#valor digitado e o maior valor digitado. O programa informa o maior e o menor, um em
#cada linha, quando o usu�rio digitar um 0.
main:	
	addi $t4,$0,0 #maior n�mero
	addi $t5,$0,0 #menor n�mero
	addi $t6,$0,0 #qtd de n�meros lidos

ler_n:	addi $2,$0,5
	syscall
	addi $t0,$2,0 #n�mero lido
	beq $t0,$0, fim
	beq $t6,$0, inicializa_maior_menor
	addi $t6,$0,1
	
ver_maior: 	slt $t2,$t4,$t0 #1, se $t4<$t0
		bne $t2,$0, atualiza_maior
		j ver_menor
atualiza_maior:  addi $t4,$t0,0
ver_menor:	slt $t2,$t0,$t5 #1, se $t0<$t5
		bne $t2,$0, atualiza_menor
		j ler_n
atualiza_menor:	 addi $t5,$t0,0
		 j ler_n
		 
inicializa_maior_menor:		addi $t4,$t0,0
				addi $t5,$t0,0
				addi $t6,$0,1
				j ler_n 
				
fim:		
		addi $2,$0,11
		addi $4,$0,'M'
		syscall
		addi $4,$0,'a'
		syscall
		addi $4,$0,'i'
		syscall
		addi $4,$0,'o'
		syscall
		addi $4,$0,'r'
		syscall
		addi $4,$0,':'
		syscall
		addi $4,$0,' '
		syscall
		addi $4,$t4,0
		addi $2,$0,1
		syscall
		
		addi $2,$0,11
		addi $4,$0,'\n'
		syscall
		
		addi $4,$0,'M'
		syscall
		addi $4,$0,'e'
		syscall
		addi $4,$0,'n'
		syscall
		addi $4,$0,'o'
		syscall
		addi $4,$0,'r'
		syscall
		addi $4,$0,':'
		syscall
		addi $4,$0,' '
		syscall
		addi $4,$t5,0
		addi $2,$0,1
		syscall
		
		
		addi $2,$0,10
		syscall	