main: 	
ler_a:	addi $2,$0,5
	syscall
	add $t0,$0,$2	#limite intervalo 01
ler_b:	addi $2,$0,5
	syscall
	add $t1,$0,$2	#limite intervalo 02
verifica_maior:		slt $t2,$t0,$t1 #1, se $t0<$t1
			beq $t2,$0,troca
			j proc
troca:	add $t2,$0,$t0 #troca de valores $t0 por $t1
	add $t0,$0,$t1 #$t0 � o menor valor
	add $t1,$0,$t2 #$t1 � o maior valor

proc:
	addi $t9,$0,2	
	div $t0,$t9
	mfhi $t2 #resto da divisao por 2
	bne $t2,$0,t0Par #Caso $t0 for �mpar
	j loop
t0Par:	add $t0,$t0,1 #fazendo de $t0 um n�mero par
loop:	slt $t2,$t1,$t0 #1 se t0<=t1
        bne $t2,$0, fim

imprime_num: 	add $4,$0,$t0
		addi $2,$0,1
		syscall
		
space:		addi $4,$0,' '
		addi $2,$0,11
		syscall
incremento:	addi $t0,$t0,2
		j loop
		
fim:		addi $2,$0,10
		syscall
