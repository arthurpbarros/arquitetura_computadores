.text
main: 	addi $2,$0,5
	syscall
	add $t8,$0,$2
	
	addi $2,$0,5
	syscall
	add $t9,$0,$2
	
imprime_primeiro_n: 	add $4,$0,$t8	
			addi $2,$0,1
			syscall

			sub $t0,$t8,$t9
			srl $t0,$t0,31
	
			beq $t8,$t9,sinal_igual
			beq $t0,$0,sinal_maior
			j sinal_menor

sinal_igual:	addi $4,$0,'='
		addi $2,$0,11
		syscall
		j imprime_segundo_n
sinal_maior:	addi $4,$0,'>'
		addi $2,$0,11
		syscall
		j imprime_segundo_n	
sinal_menor:	addi $4,$0,'<'
		addi $2,$0,11
		syscall	
imprime_segundo_n:	add $4,$0,$t9	
			addi $2,$0,1
			syscall
fim: 	addi $2,$0,10
	syscall