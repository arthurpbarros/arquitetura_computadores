.text
main:	add $2,$0,5
	syscall
	addi $t9,$0,10		#Constante 10
	add $t0,$0,$2		#Número lido
	div $t0,$t9		#Divisão por 10
unid:	mfhi $t1		#Guarda algarismo das unidades (resto)
	mflo $t0                
	div $t0,$t9
dez:	mfhi $t2		#Guarda algarismo das dezenas (resto)
	mflo $t0
	div $t0,$t9
cen:	mfhi $t3		#Guarda algarismo das centenas (resto)
	
soma:	add $4,$t1,$t2	
	add $4,$4,$t3		#Soma U+D+C
	
	add $2,$0,1
	syscall
	
	addi $2,$0,10
     	syscall
