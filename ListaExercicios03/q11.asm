#Fa�a um programa que encontre o primeiro m�ltiplo de 11, 13 ou 17 ap�s um n�mero
#fornecido pelo usu�rio.
main: 	addi $2,$0,5
	syscall
	
	addi $t0,$0,11 #Constantes  
	addi $t1,$0,13
	addi $t2,$0,17
	
restos:	div $2,$t0
	mfhi $t3
	div $2,$t1
	mfhi $t4
	div $2,$t2
	mfhi $t5
	
	sub $t0,$2,$t3
	sub $t1,$2,$t4
	sub $t2,$2,$t5
	
multi:	addi $t0,$t0,11
	addi $t1,$t1,13
	addi $t2,$t2,17
	
ver1:	slt $t4,$t0,$t1 	#1, se t0<t1
	bne $t4,$0,t0_menor	#t0 � menor
	j t1_menor	
t0_menor:	addi $t6,$t0,0
		j ver2
t1_menor:	addi $t6,$t1,0
ver2:	slt $t4,$t2,$t6
	bne $t4,$0, t2_menor #t2 � o menor
	j imprime	  
t2_menor:	addi $t6,$t2,0

imprime:	addi $4,$t6,0
		addi $2,$0,1
		syscall

		addi $2,$0,10
		syscall