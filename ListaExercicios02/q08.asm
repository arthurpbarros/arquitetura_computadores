.text
main: 	addi $2,$0,5
	syscall
	addi $t0,$2,0		

	addi $2,$0,5
	syscall
	addi $t1,$2,0
	
	addi $2,$0,5
	syscall
	addi $t2,$2,0
	
ver_dia:	addi $t9,$0,32
		slt $t3,$t0,$t9 #1, se dia < 32
		addi $t9,$0,27
		slt $t4,$0,$t0 #1, se 0 < dia 
		and $t4,$t4,$t3
		
		addi $t9,$0,1
		beq $t4,$t9, ver_mes
		j invalida
		
ver_mes:	addi $t9,$0,13
		slt $t3,$t1,$t9 #1, se mes < 13
		addi $t9,$0,0
		slt $t5,$0,$t1 #1, se 0 < mes 
		and $t5,$t5,$t3
		
		addi $t9,$0,1
		beq $t5,$t9, ver_anos 
		j invalida
		
ver_anos:	sub $t6,$t2,$0 
		srl $t6,$t6,31 #1, se ano >= 0
		beq $t6,$0, ver_relacao #os numeros de dias, meses e anos sao validos
		j invalida
						
ver_relacao:	addi $t9,$0,2
		beq $t1,$t9,fevereiro #mes � fevereiro
		j outros_meses
fevereiro:
	addi $2,$t2,0
	addi $t9,$0,400
	div $2,$t9
	mfhi $t8
	
	beq $t8,$0, bis
ver_4:	addi $t9,$0,4
	div $2,$t9
	mfhi $t8
	
	beq $t8,$0, ver_100
	j nbis
ver_100: 	addi $t9,$0,100
		div $2,$t9
		mfhi $t8
		bne $t8,$t0, bis
		j nbis
bis: 	addi $t3,$0,1
	j ver_bis
nbis:	addi $t3,$0,0
	
ver_bis:	addi $t4,$t3,29 #t4 � igual a 30 se o ano for bissexto, 29 se ano for normal
		slt $t5,$t0,$t4
		bne $t5,$0,valida #se t5 = 1, temos uma data valida em fevereiro
		j invalida	#data inv�lida
	

outros_meses:	addi $t9,$0,8
		slt $t3,$t1,$t9 #1 at� julho, 0 de agosto a dezembro (A)
		addi $t9,$0,2
		div $t1,$t9
		mfhi $t4 #1 se impar (B)
		addi $t9,$0,1
		beq $t3,$t9, ate_julho
		j apos_julho
ate_julho:	addi $t5,$0,31
		add $t5,$t5,$t4
		slt $t8,$t0,$t5
		bne $t8,$0, valida
		j invalida
apos_julho:	addi $t5,$0,32
		sub $t5,$t5,$t4
		slt $t8,$t0,$t5
		bne $t8,$0, valida
		j invalida

invalida:	addi $4,$0,'I'
		addi $2,$0,11
		syscall
		addi $4,$0,'N'	
		syscall
		addi $4,$0,'V'	
		syscall
		addi $4,$0,'�'	
		syscall
		addi $4,$0,'L'	
		syscall
		addi $4,$0,'I'	
		syscall
		addi $4,$0,'D'	
		syscall
		addi $4,$0,'A'	
		syscall
		j fim
		
		
valida: 	addi $4,$0,'V'	
		addi $2,$0,11
		syscall
		addi $4,$0,'�'	
		syscall
		addi $4,$0,'L'	
		syscall
		addi $4,$0,'I'	
		syscall
		addi $4,$0,'D'	
		syscall
		addi $4,$0,'A'	
		syscall
fim: 		addi $2,$0,10
		syscall
		
	

	
	
	
