.text
.main:	addi $2,$0,5
	syscall
	
	addi $t8,$0,3600
	div $2,$t8
	mfhi $t1 #resto = segundos restantes
.horas:	mflo $4 #quociente = horas
	
.imprime_horas:	addi $2,$0,1
		syscall
	
		addi $4,$0,':'	
		addi $2,$0,11
		syscall
	
		addi $t8,$0,60
		div $t1,$t8
.segundos:	mfhi $t1 #segundos
.minutos:	mflo $4 #minutos
	
.imprime_minutos:	addi $2,$0,1
			syscall
	
			addi $4,$0,':'	
			addi $2,$0,11
			syscall
	
.imprime_segundos:	add $4,$0,$t1
			addi $2,$0,1
			syscall
	
	
			addi $2,$0,10
			syscall
