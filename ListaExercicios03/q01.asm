.text
main:		addi $t0,$0,0 #valor do multiplo
		addi $t9,$0,30 #valor de parada
		addi $t1,$0,1 #1 se multiplo diferente de 30
loop:		addi $t0,$t0,3 #incrementa valor de multiplo em 3
		bne $t0,$t9, imp_muliplo #verifica se multiplo eh diferente de 30
		addi $t1,$0,0 #0 se multiplo igual a 30
imp_muliplo:	add $4,$0,$t0
		add $2,$0,1
		syscall
		beq $t1,$0,fim
sep:		addi $4,$0,','
		addi $2,$0,11
		syscall
		j loop
fim:		addi $2,$0,10
		syscall
