#Fa�a um programa para calcular o fatorial de um n�mero
main:	addi $2,$0,5
	syscall
	addi $t9,$2,0
	
	addi $t8,$0,1 #acumulador
	slt $t7,$t9,$0 #numero negativo
	bne $t7,$0, imp_fat
loop:	beq $t9,$0, imp_fat
	mul $t8,$t8,$t9
	addi $t9,$t9,-1
	j loop

imp_fat: 	add $4,$t8,$0
		addi $2,$0,1
		syscall
		
		addi $2,$0,10
		syscall