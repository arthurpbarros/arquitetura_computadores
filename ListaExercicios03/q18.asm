main: 	addi $2,$0,5
	syscall
	
	add $s0,$0,$2
	addi $t0,$0,19
	div $2,$t0
	mfhi $t1 #a
	
	addi $t0,$0,100
	div $2,$t0
	mflo $t2 #b
	 
	mfhi $t3 #c
	
	addi $t0,$0,4
	div $t2,$t0
	mflo $t4 #d
	mfhi $t5 #e
	
	addi $t0,$0,25
	addi $t6,$t2,8
	div $t6,$t0
	mflo $t6 #f
	
	sub $t7,$t2,$t6
	addi $t7,$t7,1
	addi $t0,$0,3
	
	div $t7,$t0
	mflo $t7 #g
	
	addi $t0,$0,19
	mul $t8,$t0,$t1
	add $t8,$t8,$t2
	sub $t8,$t8,$t4
	sub $t8,$t8,$t7
	addi $t0,$0,15
	add $t8,$t8,$t0
	addi $t0,$0,30
	div $t8,$t0
	mfhi $t8 #h
	
	addi $t0,$0,4
	div $t3,$t0
	mflo $t9 #i
	mfhi $t2 #b->k
	
	addi $t3,$0,32
	addi $t0,$0,2
	mul $t4,$t0,$t5
	add $t3,$t3,$t4
	mul $t4,$t0,$t9
	add $t3,$t3,$t4
	sub $t3,$t3,$t8
	sub $t3,$t3,$t2
	addi $t0,$0,7
	div $t3,$t0
	mfhi $t3 #c->L
	
	addi $t6,$0,0
	add $t6,$t6,$t1
	addi $t0,$0,11
	mul $t4,$t0,$t8
	add $t6,$t6,$t4
	addi $t0,$0,22 
	mul $t4,$t0,$t3
	add $t6,$t6,$t4
	addi $t0,$0,451 
	div $t6,$t0
	mflo $t6 #f->m
	
	add $t5,$0,$t8
	add $t5,$t5,$t3
	addi $t0,$0,-7
	mul $t4,$t0,$t6
	add $t5,$t5,$t4
	addi $t5,$t5,114
	addi $t0,$0,31
	div $t5,$t0
	mflo $t5 #e -> MES
	mfhi $t7 
	
	addi $t7,$t7,1 #DIA
	
	add $t1,$0,$s0
imprimePascoa:
	add $2,$0,11
	add $4,$0,'P'
	syscall
	add $4,$0,'�'
	syscall
	add $4,$0,'S'
	syscall
	add $4,$0,'C'
	syscall
	add $4,$0,'O'
	syscall
	add $4,$0,'A'
	syscall
	add $4,$0,':'
	syscall
	
	add $4,$0,$t7
	addi $2,$0,1
	syscall	
	
	addi $4,$0,'/'
	addi $2,$0,11
	syscall
	
	add $4,$0,$t5
	addi $2,$0,1
	syscall	
	
	addi $4,$0,'/'
	addi $2,$0,11
	syscall
	
	add $4,$0,$t1
	addi $2,$0,1
	syscall

verbissexto: 
	addi $s0,$0,400
	div $t1,$s0
	mfhi $s1
	
	beq $s1,$0,bissexto #%400 = 0 � bissexto 
	addi $s0,$0,4
	div $t1,$s0
	mfhi $s1
	
	beq $s1,$0,ver2 #%4=0 pode ser bissexto
	j nbissexto 
ver2:	addi $s0,$0,100
	div $t1,$s0
	mfhi $s1
	
	bne $s1,$0,bissexto #%100!=0 � bissexto
	j nbissexto
bissexto:
	addi $s0,$0,29
	j carnaval
nbissexto:
	addi $s0,$0,28
	
carnaval:	
	#nao mexer com s0,t7,t5,t1
	addi $s1,$0,47
	addi $s7,$0,4
	beq $t5,$s7, abril
	j marco
abril:	sub $s1,$s1,$t7
	addi $s6,$0,31
	sub $s6,$s6,$s1
	slt $s2,$0,$s6 #1, se $0 < $s6
	beq $s2,$0,soma #s2 menor ou igual 0, soma 
sub:	add $s2,$0,31
	sub $s0,$s2,$s0
	addi $s1,$0,3 #mes marco
	j impCarnaval
soma:	add $s0,$s0,$s6	#dia
	addi $s1,$0,2	#mes_fevereiro
	j impCarnaval
	
marco:	addi $s1,$0,47
	sub $s1,$s1,$t7
	sub $s0,$s0,$s1
	addi $s1,$0,2
impCarnaval:
	add $2,$0,11
	add $4,$0,'\n'
	syscall
	add $4,$0,'C'
	syscall
	add $4,$0,'A'
	syscall
	add $4,$0,'R'
	syscall
	add $4,$0,'N'
	syscall
	add $4,$0,'A'
	syscall
	add $4,$0,'V'
	syscall
	add $4,$0,'A'
	syscall
	add $4,$0,'L'
	syscall
	add $4,$0,':'
	syscall
	add $2,$0,1
	add $4,$0,$s0
	syscall
	add $2,$0,11
	add $4,$0,'/'
	syscall
	add $2,$0,1
	add $4,$0,$s1
	syscall
	add $2,$0,11
	add $4,$0,'/'
	syscall
	add $2,$0,1
	add $4,$0,$t1
	syscall

corpusCristi:
	#t2 dia Corpus, $t4 mes Corpus
	#t7 dia Pascoa, t5 mes Pascoa, t1 ano
	add $t9,$0,1
	addi $t4,$t5,2
	beq $t7,$t9,excecaoDia1
	addi $t2,$t7,-1
	j imprimiCorpusCristi
excecaoDia1:	addi $t2,$0,31
		
imprimiCorpusCristi: 	
	add $2,$0,11
	add $4,$0,'\n'
	syscall
	add $4,$0,'C'
	syscall
	add $4,$0,'O'
	syscall
	add $4,$0,'R'
	syscall
	add $4,$0,'P'
	syscall
	add $4,$0,'U'
	syscall
	add $4,$0,'S'
	syscall
	add $4,$0,' '
	syscall
	add $4,$0,'C'
	syscall
	add $4,$0,'H'
	syscall
	add $4,$0,'R'
	syscall
	add $4,$0,'I'
	syscall
	add $4,$0,'S'
	syscall
	add $4,$0,'T'
	syscall
	add $4,$0,'I'
	syscall
	add $4,$0,':'
	syscall
	
	add $4,$0,$t2
	addi $2,$0,1
	syscall	
	
	addi $4,$0,'/'
	addi $2,$0,11
	syscall
	
	add $4,$0,$t4
	addi $2,$0,1
	syscall	
	
	addi $4,$0,'/'
	addi $2,$0,11
	syscall
	
	add $4,$0,$t1
	addi $2,$0,1
	syscall
	
	
	 
	
	
	
	
	
	
	
	
	
	
	
	
